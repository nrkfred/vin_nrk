import json
from django.test import TestCase, Client
from rest_framework import status
from django.urls import reverse
from .models import Vin, VinDetail
from .serializers import VinDetailSerializer

# Initialize the APIClient app
client = Client()


class GetSingleVinTest(TestCase):
    """
    Test module for GET a single VIN API
    """

    def setUp(self):
        self.first_vin = Vin.objects.create(vin='1P3EW62F4VV300946')
        self.first_vin.vindetail_set.create(
            year='1997', make='Plymouth', model='Prowler', type='Pickup', color='Red',
            dimension={'width': '1.3', 'height': '1.5', 'length': '4.0'}, weight='2.7t'
        )
        self.second_vin = Vin.objects.create(vin='1P3EW65F4GV300944')
        self.second_vin.vindetail_set.create(
            year='1997', make='Plymouth', model='Prowler', type='Pickup', color='Red',
            dimension={'width': '1.6', 'height': '1.8', 'length': '4.2'}, weight='2.7t'
        )

    def test_get_single_vin(self):
        # Get API response
        response = client.get(reverse('get_vin', kwargs={'vin': self.second_vin.vin}))

        # Get data from DataBase
        vin = Vin.objects.get(vin=self.second_vin.vin)
        serializer = VinDetailSerializer(VinDetail.objects.get(vin_id=vin.id))

        # Check responses
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_vin(self):
        # Get API response
        response = client.get(reverse('get_vin', kwargs={'vin': '12345678912345678'}))

        # Check response status code
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class NotAllowedTest(TestCase):
    def setUp(self):
        self.vin = '!@#$%^&*()'

    def test_not_allowed(self):
        # Get API response
        response = client.get(reverse('not_allowed'), data={'vin': self.vin})

        # Check response status code
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
