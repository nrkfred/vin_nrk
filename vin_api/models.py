from django.db import models


class Vin(models.Model):
    vin = models.CharField(max_length=20)

    def __str__(self):
        return self.vin


class VinDetail(models.Model):
    vin = models.ForeignKey(Vin, on_delete=models.CASCADE)
    year = models.CharField(max_length=4)
    make = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    color = models.CharField(max_length=255)
    dimension = models.CharField(max_length=255)
    weight = models.CharField(max_length=255)
