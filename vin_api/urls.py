from django.conf.urls import url
from . import views

urlpatterns = [
    url(
        r'api/v1/vin/(?P<vin>[0-9A-Za-z]+)?$',
        views.get_vin,
        name='get_vin'
    ),
    url(
        r'api/v1/vin/',
        views.not_allowed,
        name='not_allowed'
    ),
    url(
        r'^$',
        views.index,
        name='index'
    ),
]