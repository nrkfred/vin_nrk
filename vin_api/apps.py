from django.apps import AppConfig


class VinApiConfig(AppConfig):
    name = 'vin_api'
