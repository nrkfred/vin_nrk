from django.contrib import admin
from .models import Vin, VinDetail

admin.site.register(Vin)
admin.site.register(VinDetail)
