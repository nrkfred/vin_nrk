from rest_framework import serializers
from .models import VinDetail


class VinDetailSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VinDetail
        fields = ('year', 'make', 'model', 'type', 'color', 'dimension', 'weight')
