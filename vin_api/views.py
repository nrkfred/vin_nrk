from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import Vin, VinDetail
from .serializers import VinDetailSerializer
import requests
from django.http import HttpResponse
from random import randint


@api_view(['GET'])
def get_vin(request, vin):
    try:
        if vin is None:
            data = {
                'Error': 'Bad request. REST Service URL: /api/v1/vin/{vin}'
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)
        elif vin and len(vin) != 17:
            data = {
                'Error': 'VIN must contains 17 characters'
            }
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

        _vin = Vin.objects.get(vin=vin)
    except Vin.DoesNotExist:
        result = decode_vin(vin)
        if not result:
            return Response({}, status=status.HTTP_404_NOT_FOUND)
        else:
            _vin = Vin.objects.get(vin=vin)

    # Get details of a single VIN
    serializer = VinDetailSerializer(VinDetail.objects.get(vin_id=_vin.id))
    return Response(serializer.data)


@api_view(['GET'])
def not_allowed(request):
    data = {
        'Error': 'VIN must contains letters and digits only'
    }
    return Response(data, status.HTTP_400_BAD_REQUEST)


def decode_vin(vin):
    # Lets imagine, that we receive response

    # response = requests.get('http://...')
    # if response.status_code == status.HTTP_200_OK:
    #     json_data = response.json()

    year = ['1995', '1999', '2003', '2012']
    make = ['Nissan', 'Mercedes-Benz', 'Jeep', 'Volkswagen']
    model = ['Fiesta', 'M3', 'Skyline', 'Cayenne']
    v_type = ['Hatchback', 'Sedan', 'MPV', 'Crossover']
    color = ['Black', 'Lightgreen', 'Gray', 'White']
    dimension = [
        {'width': '1.3', 'height': '1.5', 'length': '4.0'},
        {'width': '1.6', 'height': '1.8', 'length': '4.2'},
        {'width': '1.5', 'height': '1.6', 'length': '3.8'},
        {'width': '2.0', 'height': '1.9', 'length': '4.8'},
    ]
    weight = ['3.8t', '1.2t', '2.2t', '1.9t']

    new_vin = Vin.objects.create(vin=vin)
    obj = new_vin.vindetail_set.create(
        year=year[randint(0, 3)],
        make=make[randint(0, 3)],
        model=model[randint(0, 3)],
        type=v_type[randint(0, 3)],
        color=color[randint(0, 3)],
        dimension=dimension[randint(0, 3)],
        weight=weight[randint(0, 3)]
    )

    if obj:
        return True
    return False


def index(request):
    return HttpResponse('<h1>It works!<h1>')
