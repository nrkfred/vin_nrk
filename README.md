## VIN API


This API allows you to get information about vehicle by its VIN. Just pass it one as parameter and you'll get following 
info as JSON-formatted data:
1. Year
2. Make
3. Model
4. Type
5. Color
6. Dimensions
7. Weight

##

**To begin work with VIN API, you should do next steps:**

**1.** Clone this repository wherever you want on your computer:

```
git clone https://nrkfred@bitbucket.org/nrkfred/vin_api.git
```

**2.** Download and install [Python](https://www.python.org/)  version 3.6 or above.

**3.** Install required packages
```
pip3 install Django
pip3 install requests
pip3 install djangorestframework
pip3 install psycopg2
```

**4.** Download and install [PostgreSQL](https://www.postgresql.org/), create database named **vin_db**, owner **postgres**.

Open *terminal* or *Command Prompt* and type:

```
psql -U postgres
```

Enter password, by default password for user **postgres** is **postgres**

Then in PgSQL environment enter command:

```
CREATE DATABASE vin_db OWNER postgres;
```

Make sure that new DataBase was created and its owner is **postgres** user:

```
\l
```

You will see something similar to this table:

| DB name       | Owner         | Coding  | etc     |
| ------------- |:-------------:|:-------:|--------:|
| postgres      | postgres      | UTF8    | -----   |
| template0     | postgres      | UTF8    | -----   |
| template1     | postgres      | UTF8    | -----   |
| **vin_db**    | **postgres**  | UTF8    | -----   |


Quit PgSQL environment:

```
\q
```


**5.** Run *terminal* or *Command Prompt* from project folder.

Ensure that you are directly in **vin_nrk** folder and type command to make migrations:

```
python manage.py makemigrations
```

And apply the migrations:

```
python manage.py migrate
```

**6.** Open browser and sure that everything is work fine

```
http://127.0.0.1:8000/
```


## How to use API

After all installations and settings, read docs about 
[how to use API](https://editor.swagger.io//?&_ga=2.152292857.1109131370.1515741966-1175850748.1513330604#/)